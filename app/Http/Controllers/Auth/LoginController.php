<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $username; //username




    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->username=$this->findUsername(); //users name find korche
    }


    //user name ta nilam
    public function username()
    {
        return $this->username;
    }


    //email or useer name ar hkone akta dilay kaj korbe
    public function findUsername()
    {
        $login     = \request()->input('login');
        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        \request()->merge([$fieldType => $login]);
        return $fieldType;
    }


    //status 0 thakle, login korte parbo nah
    protected function authenticated(Request $request, $user)
    {
        if($user->status==0)
        {
            Auth::logout();
            return redirect()->route('login');
        }
    }




}
